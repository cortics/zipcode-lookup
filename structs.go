package main

type ZipData struct {
	City      string
	Office    string
	Zipcode   string
	Subcounty string
	County    string
	State     string
}
