package main

import (
	"encoding/csv"
	"encoding/json"
	"io"
	"os"
)

func jsonConvert(datafile string, jsonfile string) {
	zipdatafile, openErr := os.Open(datafile)
	checkErr(openErr)
	jsonFile, jsonFileErr := os.Create(jsonfile)
	checkErr(jsonFileErr)
	reader := csv.NewReader(zipdatafile)
	for {
		record, csvErr := reader.Read()
		if csvErr == io.EOF {
			break
		}
		checkErr(csvErr)
		zipDatum := ZipData{
			City:      record[0],
			Office:    record[1],
			Zipcode:   record[2],
			Subcounty: record[3],
			County:    record[4],
			State:     record[5],
		}
		jsondata, jsonErr := json.Marshal(zipDatum)
		checkErr(jsonErr)
		_, jsonWriteErr := jsonFile.Write(jsondata)
		checkErr(jsonWriteErr)
		_, jsonNewLineWriteErr := jsonFile.Write([]byte("\n"))
		checkErr(jsonNewLineWriteErr)
	}
	checkErr(zipdatafile.Close())
}
