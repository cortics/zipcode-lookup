package main

import (
	"fmt"
	"os"
)

func checkErr(err error) {
	if err != nil {
		fmt.Println("Fatal error! Exiting... ", err.Error())
		os.Exit(1)
	}
}
